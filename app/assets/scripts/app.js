import $ from 'jquery';
import "../styles/styles.css";
import "lazysizes";
import MobileMenu from "./modules/MobileMenu";

import RevealOnScroll from "./modules/RevealOnScroll";
import SmoothScroll from "./modules/SmoothScroll";
import ActiveLinks from "./modules/ActiveLinks";
import Modal from "./modules/Modal";
//alert('Hello world');

// Handles Mobile menu
let mobileMenu = new MobileMenu();

// Handles RevealOnScroll
new RevealOnScroll($("#our-beginning"));
new RevealOnScroll($("#departments"));
new RevealOnScroll($("#counters"));
new RevealOnScroll($("#testimonials"));

//added active link status in project
new ActiveLinks();

//added smooth scroll in header links
new SmoothScroll();

//added a modal for form
new Modal();
if(module.hot){
    module.hot.accept();
}

